// Теоретичні питання

// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

// Eкрануваняя - заміна у тексті  символів, керуючих структурою текста, на відповідні текстові підстановки.
// Може бути потрібне у ситуації, коли необхідно використовувати керуючий символ як «звичайний символ мови»,
// наприклад, для пошуку де-яких спеціальних символів []\^$. | ? * + ( )
// ----------------------------------------------------------------------------------------------------------

// 2. Які засоби оголошення функцій ви знаєте?

// Існує три наступних способи оголошення функції: Function Declaration, Function Expression та
// Named Function Expression.
// ----------------------------------------------------------------------------------------------------------

// 3. Що таке hoisting, як він працює для змінних та функцій?

// "Hoisting" - це підйом та переміщення інтерпретором JavaScript функцій, змінних або класів у верхню частину
// їхньої області перед виконанням коду. Підйом дозволяє безпечно використовувати функції в коді до їх оголошення.
//============================================================================================================

// Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане
//  на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

//  Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
//  При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
//  Створити метод getAge() який повертатиме скільки користувачеві років.
//  Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем
//  (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
//  Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

function createNewUser() {
  firstName = prompt("Please enter Your name:", "Valery");
  lastName = prompt("Please enter Your surname:", "Davidenko");
  birthDate = prompt(
    "Please enter Your date of birth (dd.mm.yyyy):",
    "09.10.1977"
  );
}

createNewUser();
alert("Hello, " + firstName + " " + lastName);

if (
  birthDate.length !== 10 ||
  birthDate === undefined ||
  birthDate === "" ||
  birthDate === null
) {
  alert("Please check Your date of birth");
  birthDate = prompt(
    "Please enter Your date of birth (dd.mm.yyyy):",
    "12.05.2000"
  );
}

dayOfBirth = +birthDate.slice(1, 2);
monthOfBirth = +birthDate.slice(3, 5);
yearOfBirth = +birthDate.slice(6, 10);

if (dayOfBirth === 0 || dayOfBirth > 31) {
  alert("Please check Your Day of birth");
  birthDate = prompt(
    "Please enter Your Date of birth (dd.mm.yyyy):",
    "12.05.2000"
  );
}

if (monthOfBirth === 0 || monthOfBirth > 12) {
  alert("Please check Your Month of birth");
  birthDate = prompt(
    "Please enter Your date of birth (dd.mm.yyyy):",
    "12.05.2000"
  );
}

if (yearOfBirth < 1900 || yearOfBirth > 2022) {
  alert("Please check Your Year of birth");
  birthDate = prompt(
    "Please enter Your date of birth (dd.mm.yyyy):",
    "12.05.2000"
  );
}

let newUser = {
  firstName: firstName,
  lastName: lastName,
  birthday: birthDate,
  get login() {
    let string = `${firstName.substring(0, 1) + lastName.toLowerCase()}`;
    login = `${string.toLowerCase()}`;
    return login;
  },
  set login(login) {},

  get age() {
    const currentTime = new Date();
    const currentDay = currentTime.getDate();
    const currentMonth = 1 + currentTime.getMonth();
    const currentYear = currentTime.getFullYear();

    if (monthOfBirth <= currentMonth) {
      ageInYears = currentYear - yearOfBirth - 1;
      ageInMonthes = currentMonth - monthOfBirth + 11;
    } else {
      ageInYears = currentYear - yearOfBirth;
      ageInMonthes = monthOfBirth - currentMonth;
    }
    /* 
    if (dayOfBirth <= currentDay) {
      ageInMonthes = currentMonth - monthOfBirth - 1;
      ageInDays = currentDay - dayOfBirth;
    } else {
      ageInMonthes = currentMonth - monthOfBirth;
      ageInDays = dayOfBirth - currentDay;
    }
 */
    const age =
      `${ageInYears + " years"}` + " " + `${ageInMonthes + " monthes"}`;
    return age;
  },

  set age(age) {},

  get password() {
    let letter = `${firstName.toUpperCase()}`;
    let str = `${letter.substring(0, 1) + lastName.toLowerCase()}`;
    password = `${str + yearOfBirth}`;
    return password;
  },
  set password(password) {},
};

console.log(newUser);
console.log(newUser.login);
console.log(newUser.password);
console.log(newUser.age);
